/* FTPDSTOR.C */
#include "httpd.h"

static int ftp_mvs_open(FTPC *ftpc, const char *fn, const char *fm, void **ret);
static int ftp_ufs_open(FTPC *ftpc, const char *fn, const char *fm, void **ret);

__asm__("\n&FUNC    SETC 'ftpfopen'");
void *
ftpfopen(FTPC *ftpc, const char *fn, const char *fm)
{
    int         rc          = 0;
    void        *fp         = NULL;
    unsigned    *psa        = (unsigned *)0;
    unsigned    *ascb       = (unsigned *)psa[0x224/4]; /* A(ASCB)      */
    unsigned    *asxb       = (unsigned *)ascb[0x6C/4]; /* A(ASXB)      */
    ACEE        **asxbsenv  = (ACEE **)  &asxb[0xC8/4]; /* A(ASXBSENV)  */
    ACEE        *oldacee    = *asxbsenv;                /* prev ACEE    */
    int         lockrc;

    /* lock the ASXB (ENQ) address */
    lockrc = lock(asxb,0);

    if (ftpc->flags & FTPC_FLAG_CWDDS || ftpc->flags & FTPC_FLAG_CWDPDS) {
        /* the current working directory is a MVS PDS or dataset level */
        rc = try(ftp_mvs_open, ftpc, fn, fm, &fp);
    }
    else {
        /* the current working directory is a UFS path name */
        rc = try(ftp_ufs_open, ftpc, fn, fm, &fp);
    }

quit:
    if (rc) {
        /* restore security environment */
        racf_set_acee(oldacee);
    }

    /* unlock the asxb */
    if (lockrc==0) unlock(asxb,0);

    return fp;
}

__asm__("\n&FUNC    SETC 'ftp_mvs_open'");
static int
ftp_mvs_open(FTPC *ftpc, const char *fn, const char *fm, void **ret)
{
    void        *fp         = NULL;
    unsigned    *psa        = (unsigned *)0;
    unsigned    *ascb       = (unsigned *)psa[0x224/4]; /* A(ASCB)      */
    unsigned    *asxb       = (unsigned *)ascb[0x6C/4]; /* A(ASXB)      */
    ACEE        *oldacee;

    /* set the security environment */
    oldacee = racf_set_acee(ftpc->acee);

    /* open requested dataset */
    fp = fopen(fn, fm);
    if (fp) {
        ftpc->fflags    = FTPC_FFLAG_MVS;       /* fp is a MVS dataset  */
    }

    /* restore security environment */
    racf_set_acee(oldacee);

quit:
    *ret = fp;
    return 0;
}

__asm__("\n&FUNC    SETC 'ftp_ufs_open'");
static int
ftp_ufs_open(FTPC *ftpc, const char *fn, const char *fm, void **ret)
{
    UFSFILE     *fp         = NULL;

    /* open requested path */
    fp = ufs_fopen(ftpc->ufs, fn, fm);
    if (fp) {
        ftpc->fflags    = FTPC_FFLAG_UFS;       /* fp is a UFS file */
    }

quit:
    *ret = fp;
    return 0;
}
