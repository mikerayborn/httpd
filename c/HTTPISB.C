/* HTTPISB.C
** See if client handle is in busy array
*/
#include "httpd.h"

/* http_is_busy() */
int
httpisb(HTTPC *httpc)
{
    CLIBGRT     *grt    = __grtget();
    HTTPD       *httpd  = grt->grtapp1;
    int         isbusy  = 0;
    unsigned    count   = array_count(&httpd->busy);
    unsigned    n;

    if (!count) goto quit;  /* nothing is in busy array */
    if (!httpc) goto quit;  /* no client handle */

    for(n=0; n < count; n++) {
        if (httpd->busy[n] == httpc) {
            isbusy++;
            break;
        }
    }

quit:
    return isbusy;
}
