/* HTTPFILE.C
** Send file, file handle is already open.
*/
#include "httpd.h"

typedef enum sstate     SSTATE;
enum sstate {
    SSTATE_SENDING=0,
    SSTATE_EOF,
    SSTATE_DONE
};

int
httpfile(HTTPC *httpc, FILE *fp, int binary)
{
    int     rc  = 0;
    int     rdw = binary ? httpc->rdw : 0;
    int     len = 0;
    int     avail;

    http_enter("httpfile()\n");

    switch (httpc->substate) {
    case SSTATE_SENDING:
        /* do we have any room in our buffer? */
        avail = CBUFSIZE - httpc->len;
        if (avail > 0) {
#if 0
            /* yes, read some data */
            len = http_read(httpc->fp, &httpc->buf[httpc->len], avail, rdw);
#else
            if (httpc->fp) {
                /* use CLIB version of fread() */
                len = fread(&httpc->buf[httpc->len], 1, avail, httpc->fp);
            }
            else if (httpc->ufp) {
                len = ufs_fread(&httpc->buf[httpc->len], 1, avail, httpc->ufp);
            }
#endif
            if (len>0) {
                /* we read some data */
#if 0
                http_dump(&httpc->buf[httpc->len], len,
                    "httpc->buf[%d]", httpc->len);
#endif
                if (!binary) {
                    /* convert to ASCII */
                    /* wtodumpf(&httpc->buf[httpc->len], len, "%s before", __func__); */
                    http_etoa(&httpc->buf[httpc->len], len);
                    /* wtodumpf(&httpc->buf[httpc->len], len, "%s after", __func__); */
                }
                httpc->len += len;
            }

            /* check for end-of-file */
            if (httpc->fp) {
                if (feof(httpc->fp)) {
                    httpc->substate = SSTATE_EOF;
                }
            }
            else if (httpc->ufp) {
                if (ufs_feof(httpc->ufp)) {
                    /* wtof("%s EOF", __func__); */
                    httpc->substate = SSTATE_EOF;
                }
            }
        }
    }

    if (httpc->pos < httpc->len) {
        /* we have unsent data in buffer */
        len = httpc->len - httpc->pos;
        if (len > 0) {
            len = http_send(httpc, &httpc->buf[httpc->pos], len);
            if (len>0) {
                httpc->pos += len;
            }
        }
    }

    if (httpc->pos>0) {
        len = httpc->len - httpc->pos;
        if (len>0) {
            /* shift data left */
            memcpy(httpc->buf, &httpc->buf[httpc->pos], len);
            /* clear remaining buffer */
            avail = CBUFSIZE - httpc->len;
            memset(&httpc->buf[len], 0, avail);
            /* update data position and length */
            httpc->pos -= len;
            httpc->len -= len;
        }
        else {
            /* reset buffer */
            memset(httpc->buf, 0, CBUFSIZE);
            httpc->pos = 0;
            httpc->len = 0;
        }
    }

    if (httpc->substate==SSTATE_EOF) {
        if (httpc->pos==0 && httpc->len==0) {
            httpc->state = CSTATE_DONE;
            httpc->substate = SSTATE_DONE;
        }
    }

    http_exit("httpfile()\n");
    return rc;
}
