/* HTTPREAD.C
** Read file
*/
#include "httpd.h"
#include "mvssupa.h"
#define FIXED_BINARY    0
#define VARIABLE_BINARY 1
#define FIXED_TEXT      2
#define VARIABLE_TEXT   3

int
httpread(FILE *fp, UCHAR *buf, int size, int rdw)
{
    int     nmemb   = -1;
    FILE    *stream = fp;
    UCHAR   *dptr;
    size_t  lenread;
    size_t  bytes;
    size_t  read;
    size_t  totalread;
    size_t  extra;
    UCHAR   *eptr;

    http_enter("httpread()\n");

    if (!fp) goto quit;
    if (!buf) goto quit;
    if (size < 1) goto quit;

    if (stream->style!=VARIABLE_BINARY) {
        nmemb = fread(buf, 1, size, stream);
        goto quit;
    }

    /* VARIABLE_BINARY style dataset */
    if (stream->eofInd) {
        nmemb = 0;
        goto quit;
    }

    bytes = size;                           /* bytes wanted */
    read = stream->endbuf - stream->upto;   /* bytes in our buffer */
#if 0
    http_dbgf("bytes=%d, read=%d\n", bytes, read);
#endif

    if (read > bytes) {
        /* we have more than they need */
#if 0
        http_dbgf("copy1 %08X, %08X, %d\n", buf, stream->upto, bytes);
#endif
        memcpy(buf, stream->upto, bytes);
        stream->upto += bytes;
        totalread = bytes;
    }
    else {
        /* copy what we have to there buffer */
#if 0
        http_dbgf("copy2 %08X, %08X, %d\n", buf, stream->upto, read);
#endif
        memcpy(buf, stream->upto, read);
        stream->bufStartR += (stream->endbuf - stream->fbuf);
        stream->upto = stream->endbuf = stream->fbuf;
        totalread = read;
    }

    while (totalread < bytes) {
        /* read from disk into internal buffer */
        if (__aread(stream->hfile, &dptr, &lenread) != 0) {
            /* end of file */
            stream->eofInd = 1;
            break;
        }
#if 0
        http_dbgf("dptr=%08X, lenread=%d\n", dptr, lenread);
#endif
        /* we read a record from the disk */
        if (!stream->reallyu) {
            /* we read from a RECFM=V style dataset */

            /* get record size from RDW */
            read = (dptr[0] << 8) BOR dptr[1];
#if 0
            http_dump(dptr, 4, "RDW Bytes");
            http_dbgf("read length from RDW=%d\n", read);
#endif

            /* fixup the RDW as needed */
            switch(rdw) {
            case RDW_NONE:
                /* remove the RDW from the buffer */
                if (read >= 4) {
                    read-=4;    /* adjust for RDW size */
                    /* shift buffer left */
                    memcpy(&dptr[0], &dptr[4], read);
                    /* clear trailing bytes */
                    dptr[read+0] = 0;
                    dptr[read+1] = 0;
                    dptr[read+2] = 0;
                    dptr[read+3] = 0;
                }
                break;
            case RDW_B2:
            case RDW_L2:
                /* we only want a 2 byte RDW X'LLLL' */
                if (read >= 4) {
                    read-=2;    /* adjust for RDW size */
                    /* correct RDW */
                    if (rdw==RDW_L2) {
                        dptr[0] = read & 0xFF;
                        dptr[1] = read >> 8;
                    }
                    else {
                        dptr[0] = read >> 8;
                        dptr[1] = read & 0xFF;
                    }

                    /* shift buffer left */
                    memcpy(&dptr[2], &dptr[4], read);

                    /* clear trailing bytes */
                    dptr[read+0] = 0;
                    dptr[read+1] = 0;
                }
                break;
            case RDW_B4:
                /* big-endian RDW 4 bytes X'LLLL', X'0000' */
                break;
            case RDW_L4:
                /* little-endian RDW 4 bytes X'LLLL', X'0000' */
                dptr[0] = read & 0xFF;
                dptr[1] = (read >> 8) & 0xFF;
                break;
            }
        }
        else {
            /* get record size returned by __aread() */
            read = lenread;

            if (stream->reallyt) {
                unsigned char *p;

                /* get rid of any trailing NULs in text mode */
                p = memchr(dptr, '\0', read);
                if (p != NULL) {
                    read = p - dptr;
                }
            }
        }

        if ((totalread + read) > bytes) {
            extra = (totalread + read) - bytes;
            read -= extra;
            memcpy(stream->fbuf, dptr + read, extra);
            stream->endbuf = stream->fbuf + extra;
        }

        memcpy((char *)buf + totalread, dptr, read);
        totalread += read;
        stream->bufStartR += read;
    }

    if (size) {
        nmemb = totalread;
    }
    else {
        nmemb = 0;
    }

quit:
    http_exit("httpread() rc=%d\n", nmemb);
    return nmemb;
}
