/* HTTPPARS.C
** Parse received headers from client.
** Transitions to next state as needed.
*/
#include "httpd.h"

static int http_set_post_env(HTTPC *httpc, const UCHAR *name, const UCHAR *value);

/* http_parse() - client state CSTATE_PARSE
** Parse header in the client buffer and create variables from
** the values we encounter.
** Transition to the next state depending on the HTTP method
** we find in the request headers (GET, HEAD, POST, ...).
*/
extern int
httppars(HTTPC *httpc)
{
    int     rc      = 0;
    UCHAR   *buf    = httpc->buf;   /* received request and headers */
    int     len     = httpc->len;   /* length of data in buffer */
    int     pos     = 0;
    UCHAR   *data   = NULL;
    int     datalen = 0;
    int     i;
    UCHAR   *p;
    int     salen;
    struct sockaddr sa;
    UCHAR   tmp[4096];

    http_enter("httppars()\n");

    /* at this point httpc->buf has ASCII headers */
    httpc->buf[CBUFSIZE-1]=0;       /* make sure we'll 0 byte terminated */

    /* check for data following the ASCII headers */
    data = strstr(buf,"\x0A\x0A");
    if (data) {
        *data = 0;
        data += 2;  /* skip over LFLF */
    }
    else {
        data = strstr(buf, "\x0D\x0A\x0D\x0A");
        if (data) {
            *data = 0;
            data += 4;  /* skip over CRLF CRLF */
        }
    }
    if (data) {
        /* calculate length of data in httpc->buf after the headers */
        datalen = (&buf[len] - data);
        len = strlen(buf);  /* update length of headers */
    }

    /* convert the ASCII headers to EBCDIC */
    http_atoe(buf,len);
#if 0
    wtof("Request:%s\n", buf);
#endif
    /* normalize any CRLF to just LF */
    for(i=0; i < len; i++) {
        if (buf[i]==0x0D) {
            /* strip the CR, shift headers left */
            strcpy(&buf[i], &buf[i+1]);
            len--;
            buf[len] = 0;
        }

        /* some code pages have a strange notion of what the newline
        ** character should be, so we'll normalize it here
        */
        if ((buf[i]==0x0A) OR (buf[i]==0x25)) {
            /* replace with our newline */
            buf[i]='\n';
        }
    }

    /* "GET path/to/resource HTTP/1.0\n" */
    p = strchr(buf, '\n');
    if (p) {
        /* convert newline to 0 byte */
        *p++ = 0;

        /* set position for header parse */
        pos = (int)(p-buf);
    }
#if 0
    wtof("Request1:%s\n", buf);
    wtof("pos=%d\n", pos);
#endif

    /* save the original request */
#if 0
    http_dbgf("HTTP_REQUEST:%s\n", buf);
#endif
    if (http_set_env(httpc, "HTTP_REQUEST", buf)) goto failed;

    /* parse request */
    p = strtok(buf, " ");
    if (!p) goto failed;
#if 0
    http_dbgf("REQUEST_METHOD:%s\n",p);
#endif
    if (http_set_env(httpc, "REQUEST_METHOD", p)) goto failed;

    p = strtok(NULL, " ");
    if (!p) goto failed;
#if 0
    http_dbgf("REQUEST_URI:%s\n",p);
#endif
    if (http_set_env(httpc, "REQUEST_URI", p)) goto failed;

    p = strtok(NULL, " ");
    if (!p) goto failed;
#if 0
    http_dbgf("REQUEST_VERSION:%s\n",p);
#endif
    if (http_set_env(httpc, "REQUEST_VERSION", p)) goto failed;

    if (pos) {
        /* parse optional headers */
        for(buf = &httpc->buf[pos]; buf[0]; buf = &httpc->buf[pos]) {
            p = strchr(buf, '\n');
            if (!p) break;

            *p++ = 0;                   /* newline -> 0 byte */
            pos = (int)(p-httpc->buf);  /* next header position */

            p = strchr(buf, ':');       /* delimiter */
            if (!p) continue;

            *p++ = 0;                   /* delimiter -> 0 byte */
#if 0
            http_dbgf("HTTP_%s:%s\n", buf, p);
#endif
            if (http_set_http_env(httpc, buf, p)) goto failed;
        }
    }

    /* parse the REQUEST_URI */
    p = http_get_env(httpc, "REQUEST_URI");
    if (!p) goto failed;

    /* prepare for query variables */
    memset(tmp, 0, sizeof(tmp));
    strcpy(tmp, p);
    buf = tmp;

    p = strchr(buf, '?');   /* any query variables? */
    if (p) {
        /* we have query variables */
        /* create "QUERY_STRING" environment variable */
        if (http_set_env(httpc, "QUERY_STRING", p+1)) goto failed;

        /* parse the query string */
        strcat(buf, "&");   /* append a final "&" to buffer */
        *p++ = 0;           /* "?" -> 0 byte */
        buf = p;            /* start of query variables */
        for (p=strchr(buf,'&'); p; buf=&buf[pos], p=strchr(buf,'&')) {
            *p++ = 0;
            pos = (int)(p - buf);
            if (!buf[0]) continue;  /* "&&" or trailing "&" */

            p = strchr(buf,'=');
            if (!p) {
#if 0
                http_dbgf("QUERY_%s:%s\n", buf, "");
#endif
                if (http_set_query_env(httpc, buf, "")) goto failed;
            }
            else {
                *p++=0;     /* "=" -> 0 byte */
#if 0
                http_dbgf("QUERY_%s:%s\n", buf, p);
#endif
                if (http_set_query_env(httpc, buf, p)) goto failed;
            }
        }
    }

    /* decode the uri and create REQUEST_PATH */
    buf = http_decode(tmp);
#if 0
    http_dbgf("REQUEST_PATH:%s\n", buf);
#endif
    if (http_set_env(httpc, "REQUEST_PATH", buf)) goto failed;

    /* create server variables for this client */
    salen = sizeof(sa);
    memset(&sa, 0, sizeof(sa));
    if (getsockname(httpc->socket, &sa, &salen)==0) {
#if 0
        http_dbgf("Raw sa_data:");
        for(p=sa.sa_data, i=0; i < sizeof(sa.sa_data); i++) {
            http_dbgf("%02X",p[i]);
        }
        http_dbgf("\n");
#endif
        p = sa.sa_data;
        sprintf(tmp, "%d.%d.%d.%d", p[2], p[3], p[4], p[5]);
#if 0
        http_dbgf("SERVER_ADDR:%s\n", tmp);
#endif
        if (http_set_env(httpc, "SERVER_ADDR", tmp)) goto failed;
        sprintf(tmp, "%d", ((p[0] << 8) + p[1]));
#if 0
        http_dbgf("SERVER_PORT:%s\n", tmp);
#endif
        if (http_set_env(httpc, "SERVER_PORT", tmp)) goto failed;
    }
    if (http_set_env(httpc, "SERVER_PROTOCOL", "HTTP/1.0")) goto failed;
    if (http_set_env(httpc, "SERVER_SOFTWARE",
        "HTTPD/1.0 (MVS38J) Lua/5.3")) goto failed;

    /* select next state based on request method */
    p = http_get_env(httpc, "REQUEST_METHOD");
    if (!p) goto failed;

    if (http_cmp(p, "GET")==0) {
        httpc->state = CSTATE_GET;
        goto nodata;
    }
    if (http_cmp(p, "HEAD")==0) {
        httpc->state = CSTATE_HEAD;
        goto nodata;
    }
    if (http_cmp(p, "PUT")==0) {
        httpc->state = CSTATE_PUT;
        goto moredata;
    }
    if (http_cmp(p, "POST")==0) {
        httpc->state = CSTATE_POST;
        goto moredata;
    }

    /* not implemented */
    rc = http_resp_not_implemented(httpc);
    httpc->state = CSTATE_DONE;
    goto quit;

failed:
    /* most likely a bad request, reset the connection */
    httpc->state = CSTATE_RESET;
    goto quit;

moredata:
    if (data && datalen < CBUFSIZE) {
        /* keep any data we found after the headers */
        memcpy(httpc->buf, data, datalen);
        memset(&httpc->buf[datalen], 0, CBUFSIZE-datalen);
        httpc->len = datalen;
        httpc->pos = 0;
        if (!datalen) goto quit;

        /* we have POST variables or PUT data */
        if (httpc->state != CSTATE_POST) goto quit;  /* can't parse non-POST data here */

        /* create "POST_STRING" environment variable */
        if (http_set_env(httpc, "POST_STRING", httpc->buf)) goto failed;

        /* convert ASCII data to EBCDIC */
        http_atoe(httpc->buf, httpc->len);

        /* parse the POST variables */
        buf = http_decode(httpc->buf);
        strcat(buf, "&");   /* append a final "&" to buffer */
        for (p=strchr(buf,'&'); p; buf=&buf[pos], p=strchr(buf,'&')) {
            *p++ = 0;
            pos = (int)(p - buf);
            if (!buf[0]) continue;  /* "&&" or trailing "&" */

            p = strchr(buf,'=');
            if (!p) {
#if 0
                http_dbgf("POST_%s:%s\n", buf, "");
#endif
                if (http_set_post_env(httpc, buf, "")) goto failed;
            }
            else {
                *p++=0;     /* "=" -> 0 byte */
#if 0
                http_dbgf("POST_%s:%s\n", buf, p);
#endif
                if (http_set_post_env(httpc, buf, p)) goto failed;
            }
        }
        /* we drop through into label nodata after processing the POST variables */
    }

nodata:
    /* no more expected data from client */
    memset(httpc->buf, 0, CBUFSIZE);
    httpc->len = 0;
    httpc->pos = 0;

quit:
    httpc->subtype = 0;
    httpc->substate = 0;
    http_exit("httppars(), rc=%d\n", rc);
    return rc;
}


static int
http_set_post_env(HTTPC *httpc, const UCHAR *name, const UCHAR *value)
{
    char        newname[256];

    sprintf(newname, "POST_%s", name);

    return http_set_env(httpc, newname, value);
}
