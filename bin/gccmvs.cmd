:: Compile C code on local machine using GCCMVS.EXE
::
:: Local stuff
@SETLOCAL ENABLEEXTENSIONS
@SET me=%~n0
@SET parent=%~dp0
@SET name=%~n1%
@SET name=%name:~0,8%
@CALL :UpCase name
::
@SET file=%1
@SET job=%name%
@SET temp=F:\%name%.JCL
@SET asm=F:\%name%.ASSEMBLE
::
:: our include directories
@SET inc=-IN:\crent370\include -IN:\httpd\include -IN:\ufs\include

:: Compile C source to assemble source code
.\bin\gccmvs.3.2.3.exe -x c -g -dA -Os %inc% -o %asm% -S %file%
@SET rc=%errorlevel%
@if %rc% GTR 0 GOTO :quit

:: Assemble output object
@SET ASMOUT=MDR.HTTPD.OBJECT(%JOB%)

:: Link output load module
@SET SYSLMOD=MDR.HTTPD.NCALIB(%JOB%)

:: Local MACLIB
@SET MAC1=MDR.CRENT370.MACLIB
::
:: Construct JCL for assemble and link
@CALL JOBCARD.JCL.CMD %job% > %temp%
@CALL GCCASM.JCL.CMD %asm% >> %temp%
@DEL %asm%
@CALL LINK.JCL.CMD >> %temp%
@CALL SUBMIT.CMD %temp%
@GOTO :quit

:UpCase
:: Subroutine to convert a variable VALUE to all UPPER CASE.
:: The argument for this subroutine is the variable NAME.
@FOR %%i IN (".=$" "a=A" "b=B" "c=C" "d=D" "e=E" "f=F" "g=G" "h=H" "i=I" "j=J" "k=K" "l=L" "m=M" "n=N" "o=O" "p=P" "q=Q" "r=R" "s=S" "t=T" "u=U" "v=V" "w=W" "x=X" "y=Y" "z=Z") DO @CALL SET "%1=%%%1:%%~i%%"
@GOTO:EOF

:quit
@ECHO %me% ended with rc=%rc%
@DEL %temp%
