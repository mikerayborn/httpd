:: Assemble module
::
:: Variables at input
::  %1          assembler source code (optional)
::  %ASMIN%     assembler source code dataset (optional)
::  %ASMOUT%    assembler object dataset (optional, defaults to temp dataset)
@ECHO //*                                                       
@ECHO //ASM      EXEC PGM=ASMBLR,                               
@ECHO //            PARM='DECK,LIST,RENT'                            
@ECHO //SYSLIB   DD DSN=SYS1.MACLIB,DISP=SHR,DCB=BLKSIZE=32720  
@ECHO //         DD DSN=SYS1.AMODGEN,DISP=SHR                   
@ECHO //         DD DSN=SYS1.UMODMAC,DISP=SHR
@ECHO //         DD DSN=SYS1.ZMACLIB,DISP=SHR
@IF NOT "%MAC1%"=="" @ECHO //         DD DSN=%MAC1%,DISP=SHR               
@IF NOT "%MAC2%"=="" @ECHO //         DD DSN=%MAC2%,DISP=SHR               
@IF NOT "%MAC3%"=="" @ECHO //         DD DSN=%MAC3%,DISP=SHR               
@IF NOT "%MAC4%"=="" @ECHO //         DD DSN=%MAC4%,DISP=SHR               
@IF NOT "%MAC5%"=="" @ECHO //         DD DSN=%MAC5%,DISP=SHR               
@IF NOT "%MAC6%"=="" @ECHO //         DD DSN=%MAC6%,DISP=SHR               
@IF NOT "%MAC7%"=="" @ECHO //         DD DSN=%MAC7%,DISP=SHR               
@IF NOT "%MAC8%"=="" @ECHO //         DD DSN=%MAC8%,DISP=SHR               
@IF NOT "%MAC9%"=="" @ECHO //         DD DSN=%MAC9%,DISP=SHR               
@ECHO //SYSUT1   DD UNIT=SYSALLDA,SPACE=(CYL,(20,10))           
@ECHO //SYSUT2   DD UNIT=SYSALLDA,SPACE=(CYL,(10,10))           
@ECHO //SYSUT3   DD UNIT=SYSALLDA,SPACE=(CYL,(10,10))           
@ECHO //SYSPRINT DD SYSOUT=*                                    
@ECHO //SYSLIN   DD DUMMY                                       
@ECHO //SYSGO    DD DUMMY                                       

@IF "%ASMOUT%"=="" GOTO:ASMTEMP
:: the object deck is written to %ASMOUT% dataset
@ECHO //SYSPUNCH DD DSN=%ASMOUT%,DISP=SHR                       
@GOTO:INPUT

:ASMTEMP
:: the object deck is written to a temp dataset
@ECHO //SYSPUNCH DD DSN=^&^&ASMOUT,DISP=(,PASS),UNIT=SYSALLDA,   
@ECHO //            DCB=(LRECL=80,BLKSIZE=6160,RECFM=FB),    
@ECHO //            SPACE=(6160,(500,500))                   
@ECHO //*

:INPUT
@IF "%ASMIN%"=="" GOTO:INPUT1
@IF "%1"=="" GOTO:GCCCTEMP
:: assembler source code in %ASMIN% dataset
@ECHO //SYSIN    DD DSN=%ASMIN%,DISP=SHR                        
@ECHO //*                                                       
@GOTO:ASMDONE
:INPUT1
:: assembler source code in file %1
@ECHO //SYSIN    DD DATA,DLM='##'
@TYPE %1
@TYPE .\bin\newline.txt
@ECHO ##
@ECHO //*                                                       
@GOTO:ASMDONE
:GCCCTEMP
:: assembler source code passed in GCCCTEMP dataset
@ECHO //SYSIN    DD DSN=^&^&GCCCTEMP,DISP=(OLD,DELETE)   
:ASMDONE
