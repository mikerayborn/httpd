:: Submit file to Hercules
::
:: Local stuff
@SETLOCAL ENABLEEXTENSIONS
@SET me=%~n0
@SET parent=%~dp0
::
@SET file=%1
@SET temp=%file%.EBCDIC
::
:: first we convert the ASCII file to EBCDIC 80 byte fixed records
.\bin\rdrprep.exe %file% %temp%
::
:: then we initialize the reader pointing at the EBCDIC 80 byte fixed record file
.\bin\httppost.exe .\bin\httppost.cfg devinit 00C %temp% ebcdic eof
::
:: Delay just 1 second
@ping 127.0.0.1 -n 2 > nul
::
:: Delete the EBCDIC 80 byte record file
@DEL %temp%
